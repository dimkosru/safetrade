var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

function extractForProduction(loaders) {
    return ExtractTextPlugin.extract('style', loaders.substr(loaders.indexOf('!')));
}

module.exports = function (options) {
    options.lint = fs.existsSync(path.join(__dirname, '/../.eslintrc')) && options.lint !== false;

    var devServerUrl = options.production ? null : 'http://localhost:8080';
    var localIdentName = options.production ? '[hash:base64]' : '[path]-[local]-[hash:base64:5]';

    var cssLoaders = 'style!css!autoprefixer'; //'style!css?localIdentName=' + localIdentName + '!autoprefixer';
    var lessLoaders = cssLoaders + '!less';
    var sassLoaders = cssLoaders + '!sass';
    var postLoaders = 'style-loader!css-loader!postcss-loader';

    if (options.production) {
        postLoaders = extractForProduction(postLoaders);
        cssLoaders = extractForProduction(cssLoaders);
        sassLoaders = extractForProduction(sassLoaders);
        lessLoaders = extractForProduction(lessLoaders);
    }

    return {
        context: path.join(__dirname, '/../src'),
        entry: {
            safetrend: './../safetrend/safetrend.es6'
        },
        output: {
            path: path.join(__dirname, '/../bundle'),
            filename: '[name].js',
            publicPath: (devServerUrl ? devServerUrl : '') + '/bundle/'
        },
        module: {
            loaders: [
                {test: /\.es6$/, loader: 'babel'},
                {test: /\.jsx$/, loaders: ['react-hot', 'babel']},
                {test: /\.css$/, loader: postLoaders},
                {test: /\.less$/, loader: lessLoaders},
                {test: /\.scss$/, loader: postLoaders},
                {test: /\.(jpe?g|png|gif|svg)(|\?[^!]*?)$/i, loaders: ['url?limit=10000', 'img?minimize']},
                {test: /\.(woff|woff2|eot|ttf)(|\?[^!]*?)$/, loader: 'url-loader'},
                {test: /\.hbs$/i, loader: 'raw-loader'},
                {test: /\.json$/, loader: 'json-loader'},
                {test: /\.ya?ml/, loader: 'json-loader!yaml-loader'},
                {test: /\.ts$/, loader: 'babel!ts-loader'}
            ],
            // preLoaders: [
            //     {test: /\.es6$/, exclude: /node_modules/, loader: 'eslint-loader'}
            // ]
        },
        resolve: {
            root: [
                path.join(__dirname, '/../src'),
                path.join(__dirname, '/../node_modules')
            ],
            fallback: __dirname
        },
        resolveLoader: {
            root: [
                path.join(__dirname, '/../node_modules'),
                path.join(__dirname, '/custom-loaders')
            ],
            alias: {
                'autoprefixer': 'autoprefixer-loader?browsers[]=last 2 version,browsers[]=IE 9'
            }
        },
        plugins: ([].concat(
            [
                new webpack.OldWatchingPlugin(),
                new webpack.optimize.CommonsChunkPlugin('common.js'),
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    jQuery: 'jquery',
                    _: 'lodash',
                    React: 'react'
                }),
                new webpack.DefinePlugin({
                    PROD: !!options.production
                }),
                new webpack.optimize.DedupePlugin()
            ], !options.production ? [] : [ // for production
                new ExtractTextPlugin('[name].css'),
                new webpack.optimize.UglifyJsPlugin({minimize: true})
            ]
        )),
        //
        // Loader configs
        // eslint: {
        //     configFile: './.eslintrc'
        // },
        imagemin: {
            gifsicle: {interlaced: false},
            jpegtran: {
                progressive: true,
                arithmetic: false
            },
            optipng: {optimizationLevel: 5},
            pngquant: {
                floyd: 0.5,
                speed: 2
            },
            svgo: {
                plugins: [
                    {removeTitle: true},
                    {convertPathData: false}
                ]
            }
        },
        postcss: function (webpack) {
            return [
                require('postcss-import')({addDependencyTo: webpack}),
                require('postcss-url')(),
                require('postcss-cssnext')(),
                require('postcss-browser-reporter')(),
                require('postcss-reporter')()
            ];
        }
    };
};
