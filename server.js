var Twig = require("twig"),
	express = require('express'),
	app = express();

Twig.cache(false);

app.get('/', function (req, res) {
	res.render('links.twig', {});
});

app.get('/:page', function (req, res) {
	res.render('pages/' + req.params.page + '.twig', {});
});


app.use('/bundle', express.static('bundle'));
app.use('/data', express.static('data'));

app.listen(9999);