import $ from 'jquery';
import './css/layout.less';
import GoogleMap from './js/GoogleMapBase.es6';
import JediValidate from 'jedi-validate';


$(function () {
	$('.js-form').each(function () {
		const warning = $('<div class="g-notification"></div>');

		$(this).prepend(warning);

		new JediValidate(this, {
			ajax: {
				sendType: 'serialize',
			},
			states: {
				error: 'has-error',
				valid: 'has-success',
				dirty: 'has-warning'
			},
			messages: {
				email: {
					email: 'Некорректный e-mail'
				},
				phone: {
					tel: 'Некорректный телефон'
				}
			},
			callbacks: {
				success: function (data) {
					try {
					warning.removeClass('g-notification_danger');
					warning.addClass('g-notification_success');
					warning.html(data.success);
					} catch (e) {

					}
				},
				error: function () {
					try {
						warning.removeClass('g-notification_success');
						warning.addClass('g-notification_danger');
						warning.html(data.error);
					} catch (e) {

					}
				}
			},
			redirect: false
		});
	});

	$('.b-map').each(function () {
		const coords = $(this).data('coords');

		new GoogleMap($(this), {
			mapOptions: {
				center: {lat: coords[0], lng: coords[1]}
			}
		});
	});

	$('.b-slider').each(function () {
		const root = $(this);
		const slidesWrapper = root.find('.b-slider__slides');
		let currentIndex = 1;
		const autoplay = !!root.data('autoplay');
		const left = root.find('.b-slider__left');
		const right = root.find('.b-slider__right');
		const caption = root.find('.b-slider__caption');
    
		let slides = root.find('.b-slider__slide');
    
		slides = root.find('.b-slider__slide');
		const bulls = root.find('.b-slider__bull');
    
		let interval = null;
    
		function startInterval() {
			if (autoplay)
				interval = setInterval(function () {
					goTo((currentIndex + 1) % slides.length);
				}, 3000);
		}
    
    
		if (autoplay)
			setTimeout(function () {
				startInterval();
			}, 1000);
		else
			goTo(currentIndex);
    
		if (left.length) {
			left.on('click', function () {
				slidesWrapper.stop(true, true);
				goTo(currentIndex);
			});
		}
		if (right.length) {
			right.on('click', function () {
				slidesWrapper.stop(true, true);
				goTo(currentIndex);
			});
		}
    
		bulls.on('click', function () {
			clearInterval(interval);
    
			setTimeout(function () {
				clearInterval(interval);
				startInterval();
			}, 1000);
    
			goTo($(this).index());
		});
    
		function goTo(index) {
            console.log(index)
            bulls.removeClass('b-slider__bull_active');
            bulls.eq(index).addClass('b-slider__bull_active');
            slides.removeClass('b-slider__slide_active');
            slides.eq(index).addClass('b-slider__slide_active');
            currentIndex = index;
        }
	});
	
	$('.b-tabs').each(function () {
		const root = $(this);

		const items = root.find('.b-tabs__item');
		const containers = root.find('.b-tabs__container');

		containers.hide();
		containers.eq(0).show();

		items.on('click', function () {
			containers.hide();
			containers.eq($(this).index()).show();

			items.removeClass('b-tabs__item_active');
			$(this).addClass('b-tabs__item_active');
		});
	});


	$('.b-video').each(function () {
		const root = $(this);
		const overlay = root.find('.b-video_overlay');
		const video = root.find('.js-video');

		video.css({
			backgroundImage: `url('https://img.youtube.com/vi/${root.data('id')}/maxresdefault.jpg')`
		});

		overlay.on('click', function () {
			overlay.fadeOut();
			video.prepend(insertVideo(root.data('type'), root.data('id')));
		});
	});

	$('.js-popup').each(function () {
		const popup = $(`		
			<div class="g-popup">
				<div class="g-popup__close icon-close"></div>
				<div class="g-popup__content">		
				</div>
			</div>`).hide();

		const content = popup.find('.g-popup__content');
		const close = popup.find('.g-popup__close');

		content.append(this);
		$('body').append(popup);

		const links = $(`[data-popup="${this.id}"]`);

		links.on('click', function () {
			popup.fadeIn();
			$('html').addClass('popup');
			return false;
		});

		close.on('click', function () {
			$('html').removeClass('popup');
			popup.fadeOut();
			return false;
		});
	})
});

function insertVideo(videoType = 'youtube', videoId = 2, width = 720, height = 480) {
	if (videoType == 'youtube') {
		return '<iframe class="youtube-player" type="text/html"'
			+ ' width="' + width + '" height="' + height + '" src="'
			+ 'http://www.youtube.com/embed/' + videoId + '?autoplay=1&rel=0&amp;controls=0&amp;showinfo=0'
			+ '" frameborder="0" wmode="opaque" autoplay="true"></iframe>';
	} else if (videoType == 'vimeo') {
		return '<iframe wmode="opaque" width="' + width + '" height="' + height + '" src="'
			+ 'http://player.vimeo.com/video/' + videoId + '?autoplay=1'
			+ '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	}

	return '';
};
