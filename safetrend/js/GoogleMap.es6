import GoogleMapBase from './GoogleMapBase.es6';

export default class GoogleMap extends GoogleMapBase {
    constructor(root, options = {}) {
        super(root, options);

        this.bounds = null;
    }

    _cacheNodes() {
        super._cacheNodes();

        $.extend(this.nodes, {
            addresses: this.root.find('.js-address')
        });
    }

    _createComponents() {
        super._createComponents();

        this.bounds = new window.google.maps.LatLngBounds();

        this.nodes.addresses.each((index, addressElement) => {
            const address = $(addressElement);

            const marker = this._createMarker({
                position: address.data('coordinates'),
                title: address.text()
            });

            this.bounds.extend(marker.getPosition());
        });
    }

    _bindEvents() {
        super._bindEvents();

        this.nodes.addresses.on('click', '.title', (event) => {
            var coordinates = $(event.currentTarget).parent().data('coordinates');

            this.map.panTo(coordinates);
        });
    }

    _ready() {
        super._ready();

        this.map.fitBounds(this.bounds);
    }
}
