import $script from 'scriptjs';

export default class GoogleMapBase {
	constructor(root, options = {}) {
		const defaultOptions = {
			mapOptions: {
				scrollwheel: false,
				zoom: 14,
				center: {lat: -25.367421, lng: 131.063741}
			},
			pin: {
				url: require('../images/pin.png'),
				size: [72, 85],
				anchor: [34, 63]
			}
		};

		this.root = root;
		this.options = _.merge(defaultOptions, options);

		this.map = null;

		$script('//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCCDsgKzIMG38QNf_jKz0Vp9iKdY2x_PJY', () => {
			this._initialize();
		});
	}

	_initialize() {
		this._cacheNodes();
		this._createComponents();
		this._bindEvents();
		this._ready();
	}

	_cacheNodes() {
		this.nodes = {
			map: this.root.find('.js-map')
		};
	}

	_createComponents() {
		this._createMap();

		this._createMarker({
			position: this.options.mapOptions.center,
			title: "Hello World!"
		});
	}

	_bindEvents() {

	}

	_ready() {

	}

	_createMap() {
		this.map = new window.google.maps.Map(this.nodes.map[0], this.options.mapOptions);
	}

	_createMarker(options) {
		let pin = null;

		if (this.options.pin) {
			pin = {
				url: this.options.pin.url,
				size: new window.google.maps.Size(this.options.pin.size[0], this.options.pin.size[1]),
				// origin: new window.google.maps.Point(this.options.pin.origin[0], this.options.pin.origin[1]),
				anchor: new window.google.maps.Point(this.options.pin.anchor[0], this.options.pin.anchor[1])
			};
		}

		const markerOptions = _.assign({
			map: this.map,
			icon: pin
		}, options);

		const marker = new window.google.maps.Marker(markerOptions);

		marker.setMap(this.map);

		return marker;
	}
}
